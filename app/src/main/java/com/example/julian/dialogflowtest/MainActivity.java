package com.example.julian.dialogflowtest;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import ai.api.AIDataService;
import ai.api.AIListener;
import ai.api.AIServiceException;
import ai.api.android.AIConfiguration;
import ai.api.android.AIService;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements AIListener {
    private AIService aiService;
    private AIDataService aiDataService;
private AIResponse test;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    private ListView ClassList;
private EditText sendtextt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ClassList = findViewById(R.id.msgList);
        sendtextt = findViewById(R.id.msg);

        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(MainActivity.this, R.layout.mylist, arrayList);
        ClassList.setAdapter(adapter);

        final ai.api.android.AIConfiguration config = new
                ai.api.android.AIConfiguration("194201fd7c0c407db021f1eea2b0dc8d",
                ai.api.android.AIConfiguration.SupportedLanguages.English,
                ai.api.android.AIConfiguration.RecognitionEngine.System);

        // Use with text search
        aiDataService = new ai.api.android.AIDataService(this, config);
        // Use with Voice input
        aiService = AIService.getService(this, config);

        aiService.setListener(this);

        send("hello");
    }

    public void SendClick(View view) {
        send(sendtextt.getText().toString());
    }
    public void send(String n) {
        AIRequest aiRequest = new AIRequest();

        aiRequest.setQuery(n);

        if(aiRequest==null) {
            throw new IllegalArgumentException("aiRequest must be not null");
        }

        final AsyncTask<AIRequest, Integer, AIResponse> task =
                new AsyncTask<AIRequest, Integer, AIResponse>() {
                    private AIError aiError;

                    @Override
                    protected AIResponse doInBackground(final AIRequest... params) {
                        final AIRequest request = params[0];
                        try {
                            final AIResponse response =    aiDataService.request(request);
                            // Return response
                            test = response;
                            return response;
                        } catch (final AIServiceException e) {
                            aiError = new AIError(e);
                            return null;
                        }
                    }

                    @Override
                    protected void onPostExecute(final AIResponse response) {
                        if (response != null) {
                            onResult(response);
                        } else {
                            onError(aiError);
                        }
                    }
                };
        task.execute(aiRequest);
    }

    @Override
    public void onResult(AIResponse result) {
        Result yes = test.getResult();
        String speech = yes.getFulfillment().getSpeech();
        arrayList.add(speech);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onError(AIError error) {

    }

    @Override
    public void onAudioLevel(float level) {

    }

    @Override
    public void onListeningStarted() {

    }

    @Override
    public void onListeningCanceled() {

    }

    @Override
    public void onListeningFinished() {

    }
}
